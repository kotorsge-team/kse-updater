﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace KSE
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private Mutex _mutex;
		private MainWindow _mw;

		private void Application_Startup(object sender, StartupEventArgs e)
		{
			// Generate mutex attributes
			var assem = Assembly.GetExecutingAssembly();
			bool mutexCreated;
			var mutexName = string.Format(CultureInfo.InvariantCulture, "Local\\{{{0}}}{{{1}}}",
				assem.GetType().GUID, assem.GetName().Name);

			// Create mutex with attributes
			_mutex = new Mutex(true, mutexName, out mutexCreated);

			// Check if a mutex already exists or not.
			// If a mutex exists, one cannot be created for the new instance
			// So kill the application
			if(!mutexCreated)
			{
				_mutex = null;
				MessageBox.Show("You can only have one updater instance open at a time", "Multi-Instance Error",
					MessageBoxButton.OK, MessageBoxImage.Error);
				Current.Shutdown();
				return;
			}

			// Mutex creation successfull, let's process given arguments from KSE
			// and launch a window if a new version is released
			_mw = new MainWindow();
			_mw.Show();
		}

		private void Application_Exit(object sender, ExitEventArgs e)
		{
			if (_mutex == null) return;
			_mutex.ReleaseMutex();
			_mutex.Close();
			_mutex = null;
		}
	}
}
